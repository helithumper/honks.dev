---
title: Introduction
type: docs
---

# Honks.dev Notes
Morally ambiguous notes on things going on.
![Honk](/img/goose_pipe.jpg)

## Why did I make this?
Why not? I had a bunch of links sitting around that I thought would be fun to share!

## Why not just use ____? instead of Hugo
Because I wanted to, that's why.

## Who should I contact if I want to add things to this website?
Add an issue to [the repo for this site](https://gitlab.com/helithumper/honks.dev)

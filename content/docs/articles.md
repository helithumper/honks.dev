---
weight: 1
#bookFlatSection: true
title: "Articles"
type: docs
---

# Interesting Articles

On this page is a list of interesting articles I have come across.

## Cybersecurity

- [Building Fast Fuzzers](https://arxiv.org/pdf/1911.07707.pdf) is a paper out of Germany describing a more efficient mechanism for Fuzzing. I honestly have not read through this yet but it looked cool ¯\\_(ツ)_/¯. I have been told that [Gamozolabs'](https://gamozolabs.github.io/) [review of this paper he did on a Twitch stream](https://m.youtube.com/watch?v=ZfuRDwEUg_Q) is really good.

---

## Computer Science

### Browsers

- [Browser Startup Comparison](https://www.netmeister.org/blog/browser-startup.html) is an article showing exactly what happens when you first launch a browser. The thing I found most interesting was the insane amount of requests towards telemetry services.

### Microservices

- [So you want to learn microservices?](https://dev.to/kgoralski/deep-dive-into-microservices-architecture-h54) is a [dev.to](https://dev.to) article explaining what microservices are and how they _should_ be developed in the authors opinion. Contains lots of links to other articles on the topic.
- [Decoupling database migrations from server startup: why and how](https://pythonspeed.com/articles/schema-migrations-server-startup/) describes a smart solution to working with [Python ORM](https://www.fullstackpython.com/object-relational-mappers-orms.html) migrations in containerized applications.

### Rust

- [A Half Hour to Learn Rust](https://fasterthanli.me/blog/2020/a-half-hour-to-learn-rust/) is an excellent article explaining most of the major mechanisms in Rust within a 30-minute reading timespan.
- [Working with strings in Rust](https://fasterthanli.me/blog/2020/working-with-strings-in-rust/) is a very long and thorough explaination of memory-safe string handling in Rust and C.
- [Rust dependencies scare](https://blog.debiania.in.ua/posts/2020-03-01-rust-dependencies-scare.html) has a good explaination of the current issues with dependency lifecycles in Rust. {March 1st 2020}
- [Writing an OS in Rust](https://os.phil-opp.com/) is a blog series focused on developing a small OS in Rust. This includes code samples as well as commentry.

### Compiler Theory

- [From Laptop to Lambda: Outsourcing Everyday Jobs to Thousands of Transient Functional Containers](https://www.usenix.org/system/files/atc19-fouladi.pdf) is a Usenix conference paper on a compiler using AWS Lambda for fast (but expensive) compilation.

### GoLang

- [Ultimate Setup for Your Next Golang Project](https://martinheinz.dev/blog/5) describes a standard designed by the writer for creating new Golang projects. Especially surrounding dependencies.
- [Go + Services = One Goliath Project](https://engineering.khanacademy.org/posts/goliath.htm) is an article by Kahn Academy about how Khan Academy is creating Golang-based microservices and their transition from Python. 

### Conatiners & Docker

- [#lifeprotip: Use Docker to simplify development workflows](https://bytes.yingw787.com/posts/2020/02/27/docker_as_vagrant/) shows how to build a development environment with Docker. I'm not sure if I agree with their methodology on this, but it is an interesting take on Docker for developers.
- [Traefik on Docker for Web Developers](https://jtreminio.com/blog/traefik-on-docker-for-web-developers/) is a really good article I found describing how to get Traefik up and running. It has a good reference for the basic Traefik configuration, but I would reccomend [the latest official documentation](https://docs.traefik.io/) for more details.

### Kubernetes

- [Events, the DNA of Kubernetes](https://www.mgasch.com/post/k8sevents/) does a good job of explaining the event system that drives Kubernetes' state management system.

### WebDev

- [How to host a static website using AWS S3 and Cloudflare](https://miketabor.com/how-to-host-a-static-website-using-aws-s3-and-Cloudflare/) is the guide I used for setting up this site with Cloudflare and AWS S3.

### Testing

- [Necessary & Sufficient](https://blog.testdouble.com/posts/2020-02-25-necessary-and-sufficient/) is a blog post describing how to better optimize a testing suite. The article focuses on the big questions of "Why Test?" and "What tests are needed?".

### Linux

- [How do you organize your home directory](https://lobste.rs/s/zpw6py/how_do_you_organize_your_home_directory) is a Lobste.rs article with many responses on how users organize their home directories

- [Flight rules for Git](https://github.com/k88hudson/git-flight-rules) is a set of git commands for when things go wrong.

- [Rusty's API Design Manifesto](http://sweng.the-davies.net/Home/rustys-api-design-manifesto) is a short description of Rusty's API levels. This is a good demonstration of how documentation and implementation lead to proper or improper usage of a program.

- [Dynamic Linking with Drew DeVault](https://drewdevault.com/dynlib.html) is a clean and straightforward analysis of binary linking and the performance surrounding linking. It comes with a really good example of how to use scripts to prove this as well as clean charts.

### Architecture

- [Building data liberation infrastructure](https://beepb00p.xyz/exports.html) is a design for a system where data is not siloed by large companies.

### Python

- [Comprehensive Python Cheatsheet](https://gto76.github.io/python-cheatsheet/) This is a really _comprehensive_ cheatsheet for what I think of when someone says "I know Python." This includes usage of most of the standard datastructures as well as libraries (Pandas in particular).

---

## Learning

- [The Cornell Note-Taking System](http://lsc.cornell.edu/wp-content/uploads/2016/10/Cornell-NoteTaking-System.pdf) is the actual Cornell University standard for the Cornell note-taking system.

---

## Writing

- [How to Write Good Documentation (and it's essential elements)](https://www.sohamkamani.com/blog/how-to-write-good-documentation/) is an amazing article on how documentation _should_ be structured as well as an amazing visual on what should be included in a project's `README.md`
- [Writing a Book with Pandoc, Make, and Vim](https://keleshev.com/my-book-writing-setup/) is a setup for writing formal books using Markdown. This provides really nice typesetting and formatting for documentation and would be useful for creating educational content.

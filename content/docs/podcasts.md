---
weight: 1
#bookFlatSection: true
title: "Podcasts"
type: docs
---

# Podcasts

This is a list of my favorite podcasts.

## Tested

My favorite two podcasts are hosted by the team at [Tested.com](https://tested.com)

{{< columns >}}
### ENDED: [Still Untitled: The Adam Savage Project](https://www.tested.com/still-untitled-the-adam-savage-project/)

![Still Untitled](/img/podcasts/stilluntitled.jpg)

As of January 2021, Still Untitled has ended. I still reccomend the backlog though.

<--->

### [This is Only a Test](http://www.tested.com/podcast-xml/this-is-only-a-test/)

![This is Only A Test](/img/podcasts/tested.jpg)

{{< /columns >}}

## Technology

{{< columns >}}
### [Kubernetes Podcast from Google](https://kubernetespodcast.com/)

![Kubernetes Podcast from Google](/img/podcasts/k8s_podcast.png)

<--->

### [Talk Python to Me](https://talkpython.fm)

![Talk Python to Me](/img/podcasts/talkpython.png)

{{< /columns >}}

## Economics / News

{{< columns >}}

### [Freakonomics Radio](https://freakonomics.com/)

![Freakonomics Radio](/img/podcasts/freakonomics.jpg)

<--->

### [The Economist Radio](https://www.economist.com/podcasts/)

![The Economist Radio](/img/podcasts/economist.png)

{{< /columns >}}

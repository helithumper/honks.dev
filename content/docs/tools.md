---
weight: 1
#bookFlatSection: true
title: "Tools"
type: docs
---

# Useful Tools

This is a list of useful tools I have come across, enjoy :)

## Development

### CNCF / Kubernetes

- [NATS.io](https://docs.nats.io/) is a distributed message queuing system in the CNCF incubator. I want to learn how it compares and contrasts with RabbitMQ and Kafka.

### IDEs

- [Theia](https://github.com/eclipse-theia/theia) is a Typescript implementation of VSCode for use in the browser. This could be useful for workshops or other interactive events.

### Python

- [WeMake Python Package](https://github.com/wemake-services/wemake-python-package) is a cookiecutter template for Python packages.
- [Poetry: Dependency Management for Python](https://github.com/python-poetry/poetry) is a dependency manager for python projects. It's goal is to unify python project management in one file: `pyproject.toml`.
- [PyCharm](https://www.jetbrains.com/pycharm/) is a great IDE for python development. The two primary features I love are the ability to create complex build configurations as well as the amazing language features and dropdowns provided by the tool. Of course, if you download this you **MUST** also download the [Nyan Cat Progress Bar Plugin](https://plugins.jetbrains.com/plugin/8575-nyan-progress-bar).

### Rust

- [mdBook](https://github.com/rust-lang/mdBook) is the tool which the Rust community uses to host Markdown files is a nice way.
- [zoxide](https://github.com/ajeetdsouza/zoxide) is a `cd`-like tool which can do fuzziy finding as well as other operations. 
- [cargo audit](https://github.com/RustSec/cargo-audit) audits your `Cargo.lock` files to check for known security vulnerabilities.

---

## Thinking / Problem Solving

- [untools](https://untools.co/) is a collection of problem solving tools which allows for more clear and structured thinking.

---

## Writing

- [Ulysses](https://ulysses.app/) is a writing tool made for the Apple ecosystem. The primary benefit I get out of this application is the ability to set writing goals. This makes it so that I can say "I want to write 3 pages" and it tells me my status along the way via a small circle timer. 

---

## Videos

- [Conferences.digital](https://github.com/zagahr/Conferences.digital) is an open source platform for viewing conference talks.

---

## Security

- [Lulu](https://github.com/objective-see/LuLu) is a MacOS Firewall Implementation.
- [Bandwhich](https://github.com/imsnif/bandwhich) is a CLI tool for viewing network utiliziation by process and hostname.
- [pspy](https://github.com/DominicBreuker/pspy) is a CLI tool to watch linux processes even if you aren't root. Very useful for privelege escalation.

---
weight: 1
#bookFlatSection: true
title: "Websites"
type: docs
---

# Websites

This is a list of cool websites I have stumbled across.

## Cybersecurity

### Malware Analysis

- [REMnux Documentation](https://remnux.org/docs/) is the documentation for REMnux, a linux malware RE and analysis toolkit.
- [VirusShare.com](https://virusshare.com) is an online repository of malware for analysis.

---

## Computer Science

### Fun things to do

- [Advent of Code](https://adventofcode.com/) is a yearly programming competition designed as an Advent calendar.

### Design

- [Refactoring UI: the Book](https://refactoringui.com/book/) is an upcoming book on UI design. Looking at their examples this could be a really interesting look into how Web UIs are designed for user interaction.

### AI/ML

- [Machine Learning Crash Course](https://developers.google.com/machine-learning/crash-course) is an online course held by Google on Machine Learning.

<!-- ### K8S -->
<!-- - [kubernet.io](http://www.kubernet.io/) Is down :C -->

---

## Video Games

- [GeForce Now Games List](https://www.gfnlist.com/changelog/) is a list of changes to the games available via GeForce Now.

---

## Feeds

- [Serializer.io](https://serializer.io/) is a combination feed of [Lobste.rs](https://lobste.rs), [HackerNews](https://news.ycombinator.com), and other feeds. It's nice to leave open on a second monitor to get pretty up to date tech news.

---

## Other Wikis / Knowledgedumps

- [Nikita Volobeov's Wiki](https://wiki.nikitavoloboev.xyz) is possibly the reason I started doing this. It's very open and has an _insane_ amount of information contained in it.

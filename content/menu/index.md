---
headless: true
---

## Useful Lists

- [Articles]({{< relref "/docs/articles" >}})
- [Podcasts]({{< relref "/docs/podcasts">}})
- [Tools]({{< relref "/docs/tools" >}})
- [Websites]({{< relref "/docs/websites" >}})

---

## Code

- [Github](https://github.com/helithumper)
- [Gitlab](https://gitlab.com/helithumper)

---

<!-- - [Blog]({{< relref "/posts" >}}) -->